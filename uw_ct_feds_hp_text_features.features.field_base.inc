<?php

/**
 * @file
 * uw_ct_feds_hp_text_features.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_ct_feds_hp_text_features_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_title_path_lookup'.
  $field_bases['field_title_path_lookup'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_title_path_lookup',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'entity_translation_sync' => FALSE,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  // Exported field_base: 'field_url_path_lookup'.
  $field_bases['field_url_path_lookup'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_url_path_lookup',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'nid' => array(
        0 => 'nid',
      ),
    ),
    'locked' => 0,
    'module' => 'node_reference',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'referenceable_types' => array(
        'biblio' => 0,
        'contact' => 0,
        'feds_club' => 'feds_club',
        'feds_contacts' => 'feds_contacts',
        'feds_employment_volunteer' => 'feds_employment_volunteer',
        'feds_event_listing_header' => 0,
        'feds_events' => 'feds_events',
        'feds_footer' => 0,
        'feds_homepage_hero_image' => 'feds_homepage_hero_image',
        'feds_homepage_main_feature' => 0,
        'feds_news' => 'feds_news',
        'feds_promo_banner' => 0,
        'feds_service_directory' => 'feds_service_directory',
        'feds_web_page_with_hero' => 'feds_web_page_with_hero',
        'feds_writers' => 'feds_writers',
        'homepage_text_feature' => 0,
        'schemaorg_event' => 0,
        'uw_blog' => 0,
        'uw_ct_person_profile' => 0,
        'uw_embedded_call_to_action' => 0,
        'uw_embedded_facts_and_figures' => 0,
        'uw_embedded_timeline' => 0,
        'uw_event' => 0,
        'uw_home_page_banner' => 0,
        'uw_image_gallery' => 0,
        'uw_news_item' => 0,
        'uw_project' => 0,
        'uw_promotional_item' => 0,
        'uw_service' => 0,
        'uw_site_footer' => 'uw_site_footer',
        'uw_special_alert' => 'uw_special_alert',
        'uw_web_form' => 'uw_web_form',
        'uw_web_page' => 'uw_web_page',
        'uwaterloo_custom_listing' => 0,
        'webform' => 0,
      ),
      'view' => array(
        'args' => array(),
        'display_name' => '',
        'view_name' => '',
      ),
    ),
    'translatable' => 0,
    'type' => 'node_reference',
  );

  // Exported field_base: 'field_url_title'.
  $field_bases['field_url_title'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_url_title',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
