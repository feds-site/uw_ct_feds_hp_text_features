<?php

/**
 * @file
 * uw_ct_feds_hp_text_features.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_hp_text_features_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_homepage_text_feature_section';
  $context->description = 'Feds homepage text feature view';
  $context->tag = 'Feds';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_text_feature-block' => array(
          'module' => 'views',
          'delta' => 'feds_text_feature-block',
          'region' => 'feature_text',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Feds homepage text feature view');
  $export['feds_homepage_text_feature_section'] = $context;

  return $export;
}
