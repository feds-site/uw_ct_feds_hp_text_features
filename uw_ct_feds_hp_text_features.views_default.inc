<?php

/**
 * @file
 * uw_ct_feds_hp_text_features.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_hp_text_features_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_text_feature';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds text feature';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Feds text feature';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title path lookup */
  $handler->display->display_options['fields']['field_title_path_lookup']['id'] = 'field_title_path_lookup';
  $handler->display->display_options['fields']['field_title_path_lookup']['table'] = 'field_data_field_title_path_lookup';
  $handler->display->display_options['fields']['field_title_path_lookup']['field'] = 'field_title_path_lookup';
  $handler->display->display_options['fields']['field_title_path_lookup']['label'] = '';
  $handler->display->display_options['fields']['field_title_path_lookup']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_title_path_lookup']['alter']['text'] = '<a href="[field_title_path_lookup-url]">[field_title_path_lookup-title] </a>';
  $handler->display->display_options['fields']['field_title_path_lookup']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_title_path_lookup']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_title_path_lookup']['click_sort_column'] = 'url';
  /* Field: Content: Category path lookup */
  $handler->display->display_options['fields']['field_url_path_lookup']['id'] = 'field_url_path_lookup';
  $handler->display->display_options['fields']['field_url_path_lookup']['table'] = 'field_data_field_url_path_lookup';
  $handler->display->display_options['fields']['field_url_path_lookup']['field'] = 'field_url_path_lookup';
  $handler->display->display_options['fields']['field_url_path_lookup']['label'] = '';
  $handler->display->display_options['fields']['field_url_path_lookup']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_url_path_lookup']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_url_path_lookup']['type'] = 'node_reference_path';
  $handler->display->display_options['fields']['field_url_path_lookup']['settings'] = array(
    'alias' => 1,
    'absolute' => 0,
  );
  /* Field: Content: Category Title */
  $handler->display->display_options['fields']['field_url_title']['id'] = 'field_url_title';
  $handler->display->display_options['fields']['field_url_title']['table'] = 'field_data_field_url_title';
  $handler->display->display_options['fields']['field_url_title']['field'] = 'field_url_title';
  $handler->display->display_options['fields']['field_url_title']['label'] = '';
  $handler->display->display_options['fields']['field_url_title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_url_title']['alter']['text'] = '<a href="[field_url_path_lookup]">[field_url_title]</a>';
  $handler->display->display_options['fields']['field_url_title']['element_label_colon'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['preserve_tags'] = '<b><strong><i><em><br>';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '400',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'homepage_text_feature' => 'homepage_text_feature',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['feds_text_feature'] = array(
    t('Master'),
    t('Feds text feature'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('<a href="[field_title_path_lookup-url]">[field_title_path_lookup-title] </a>'),
    t('<a href="[field_url_path_lookup]">[field_url_title]</a>'),
    t('Block'),
  );
  $export['feds_text_feature'] = $view;

  return $export;
}
