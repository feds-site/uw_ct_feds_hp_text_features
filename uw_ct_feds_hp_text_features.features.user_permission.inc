<?php

/**
 * @file
 * uw_ct_feds_hp_text_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_hp_text_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create homepage_text_feature content'.
  $permissions['create homepage_text_feature content'] = array(
    'name' => 'create homepage_text_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any homepage_text_feature content'.
  $permissions['delete any homepage_text_feature content'] = array(
    'name' => 'delete any homepage_text_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own homepage_text_feature content'.
  $permissions['delete own homepage_text_feature content'] = array(
    'name' => 'delete own homepage_text_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any homepage_text_feature content'.
  $permissions['edit any homepage_text_feature content'] = array(
    'name' => 'edit any homepage_text_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own homepage_text_feature content'.
  $permissions['edit own homepage_text_feature content'] = array(
    'name' => 'edit own homepage_text_feature content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter homepage_text_feature revision log entry'.
  $permissions['enter homepage_text_feature revision log entry'] = array(
    'name' => 'enter homepage_text_feature revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_text_feature authored by option'.
  $permissions['override homepage_text_feature authored by option'] = array(
    'name' => 'override homepage_text_feature authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_text_feature authored on option'.
  $permissions['override homepage_text_feature authored on option'] = array(
    'name' => 'override homepage_text_feature authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_text_feature promote to front page option'.
  $permissions['override homepage_text_feature promote to front page option'] = array(
    'name' => 'override homepage_text_feature promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_text_feature published option'.
  $permissions['override homepage_text_feature published option'] = array(
    'name' => 'override homepage_text_feature published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_text_feature revision option'.
  $permissions['override homepage_text_feature revision option'] = array(
    'name' => 'override homepage_text_feature revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_text_feature sticky option'.
  $permissions['override homepage_text_feature sticky option'] = array(
    'name' => 'override homepage_text_feature sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search homepage_text_feature content'.
  $permissions['search homepage_text_feature content'] = array(
    'name' => 'search homepage_text_feature content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
